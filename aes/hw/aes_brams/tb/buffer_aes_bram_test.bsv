import aes_bramfifo :: *;

typedef enum {
	Start2, Blockstart, Ready2, Idle2
} AES_Block_State_type deriving(Bits, Eq, FShow);

module mkAes_Block_test(Empty);
	Reg#(AES_Block_State_type) rg_state <- mkReg(Idle2);
	AES_block blockaes <- mkAesBlockCipher;
	Reg#(Aes_key_type) aeskeylen <- mkReg(Bit128);
	Reg#(Int#(32)) cl_cycle <- mkReg(0);
	Reg#(Int#(32)) ctr <- mkReg(0);
	Reg#(Int#(32)) text <- mkReg(0);
	Reg#(Bit#(128)) in1 <- mkReg(0);
	Reg#(Bit#(128)) in2 <- mkReg(0);
	Reg#(Bit#(128)) in3 <- mkReg(0);
	Reg#(Bit#(128)) in4 <- mkReg(0);
	Reg#(Bit#(128)) out1 <- mkReg(0);
	Reg#(Bit#(128)) out2 <- mkReg(0);
	Reg#(Bit#(128)) out3 <- mkReg(0);
	Reg#(Bit#(128)) out4 <- mkReg(0);
	Reg#(Bit#(256)) key <- mkReg(0);
	Reg#(Bit#(128)) iv <- mkReg(0);
	Reg#(Mode) _mode_ <- mkReg(ECB);
	
	rule clk_cycle;
	cl_cycle <= cl_cycle + 1;
	endrule
	
	rule idlecase(rg_state == Idle2);
		text <= 0;
		rg_state <= Start2;
		// change value of keyl to change keylength
		let keyl = Bit128;
		aeskeylen <= keyl;
		// change this value md to change mode
		Mode md=CBC;
		_mode_ <=md;
		$display("Hello this is shakti crypto box AES 4 block of plain text input\n");
		$display("           mode: ",fshow(md));
		$display("     Key length: ",fshow(keyl));
		//key value can be changed here
		Bit#(256) keyin= 256'h34753778214125442a462d4a614e6452;	
		key <=keyin;
		$display("            key: %h",keyin);
		//IV value can be changed here
		Bit#(128) in = 128'h432a462d4a614e645266556a586e3272;
		iv <=in;

		blockaes.set_key_iv_mode_len(key,in, False, md, keyl);

		$display("             IV: %h",in);
		//plaintext block 1 value can be changed here
		in = 128'h48656c6c6f2074686973206973207368;
		in1 <= in;
		$display("     plain text: %h",in);
		//plaintext block 2 value can be changed here
		in = 128'h616b74692063727970746f20626f7820;
		in2 <= in;
		$display("                 %h",in);
		//plaintext block 3 value can be changed here
		in = 128'h414553203420626c6f636b206f662070;
		in3 <= in;
		$display("                 %h",in);
		//plaintext block 4 value can be changed here
		in = 128'h6c61696e207465787420696e7075740a;
		in4 <= in;
		$display("                 %h",in);
		
	endrule

	rule run(rg_state == Start2);
	case(text)
	0 : begin 
		blockaes.start(in1,key,iv, False, _mode_, aeskeylen);
		$display("\n Encrypt result:                                  %d",cl_cycle);
	end
	1 : begin 
		blockaes.start(out1,key,iv, True, _mode_, aeskeylen);
		$display("\n Decrypt result:                                  %d",cl_cycle);
	end
	endcase
		rg_state <= Ready2;
	endrule
	
	
	rule ret(rg_state == Ready2);
		
		rg_state <= Blockstart;
		Bit#(128) res <- blockaes.get();
		$display("                 %h,%d",res,cl_cycle);
		case(ctr)
		0 : out1 <= res;
		1 : out2 <= res;
		2 : out3 <= res;
		3 : out4 <= res;
		endcase
		ctr <= ctr + 1;
	endrule
	
	rule blockrun(rg_state == Blockstart);
	//$display("Block start");
	case(ctr)
	1 : begin 
		blockaes.put(in2);
		rg_state <= Ready2;
	end
	2 : begin 
		blockaes.put(in3);
		rg_state <= Ready2;
	end
	3 : begin 
		blockaes.put(in4);
		rg_state <= Ready2;
	end
	4 : begin 
		blockaes.end_of_text;
		rg_state <= Start2;
		text <= text +1;
	end
	5 : begin 
		blockaes.put(out2);
		rg_state <= Ready2;
	end
	6 : begin 
		blockaes.put(out3);
		rg_state <= Ready2;
	end
	7 : begin 
		blockaes.put(out4);
		rg_state <= Ready2;
	end
	8 : begin
		blockaes.end_of_text;
		$display("Total clock cycles :%d",cl_cycle);
		$finish(0);
	end
	endcase
		
	endrule
endmodule
