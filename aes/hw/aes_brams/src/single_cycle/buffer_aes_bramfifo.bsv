package buffer_aes_bramfifo;
import buffer_aes_block_cipher :: *;
import FIFOF ::*;
import BRAMFIFO ::*;
export Aes_key_type (..) ;
export Mode (..) ;
export buffer_aes_bramfifo ::*;

interface AES_buffer#(numeric type depth);
      method Action set_iv_key_mode_len(Bit#(256) key, Bit#(128) iv,Bool decrypt, Mode mode, Aes_key_type keylenn);
      method Action set_inp(Bit#(64) intext);
      method Action end_of_text();
      method Bool can_take_inp();
      method Bool outp_ready();
      method ActionValue#(Bit#(64)) get();
endinterface

module mkAes_Buffer(AES_buffer#(depth));
	AES_block blockaes <- mkAesBlockCipher;

  FIFOF#(Bit#(128)) bram_inp <- mkSizedBRAMFIFOF(fromInteger(valueOf(depth))*2);
  FIFOF#(Bit#(128)) bram_out <- mkSizedBRAMFIFOF(fromInteger(valueOf(depth))*2);

  Reg#(Bit#(TLog#(TMul#(depth,2)))) outbuf_ctr <- mkReg(0);

	Reg#(Aes_key_type) aeskeylen <- mkReg(Bit128);
	Reg#(Bit#(1)) ctr <- mkReg(0);
	Reg#(Bit#(1)) ctr_op <- mkReg(0);
	Reg#(Bit#(64)) inp <- mkReg(0);
	Reg#(Bit#(128)) op <- mkReg(0);
	Reg#(Bit#(256)) key_aes <- mkReg(0);
	Reg#(Bit#(128)) iv_aes <- mkReg(0);
	Reg#(Mode) mode_aes <- mkReg(ECB);
  Reg#(Bool) decrypt_aes <- mkReg(False);
  Reg#(Bool) started <- mkReg(True);
  Reg#(Bool) op_ready <- mkReg(False);

  rule send_bram(!started);
    Bit#(128) temp = bram_inp.first();
    blockaes.start(temp,key_aes, iv_aes, decrypt_aes, mode_aes, aeskeylen);
    bram_inp.deq();
    $display("AES engine set iv: %h, %h, %h", temp, key_aes, iv_aes);
    started <= True;
  endrule
  rule put_bram(started);
    Bit#(128) temp = bram_inp.first();
    $display("AES engine start: %h", temp);
    blockaes.put(temp);
    bram_inp.deq();
  endrule

  rule get_aes;
    Bit#(128) res <- blockaes.get();
    $display("AES engine result: %h", res);
    bram_out.enq(res);
    outbuf_ctr <= outbuf_ctr+1;
  endrule

  rule outp((!op_ready) && (bram_out.notEmpty));
    op <= bram_out.first();
    bram_out.deq();
    op_ready<=True;
  endrule

  //rule p;
  //  $display("AES: OUTcTR, %h",outbuf_ctr);
  //endrule

  method Action set_iv_key_mode_len(Bit#(256) key, Bit#(128) iv,Bool decrypt, Mode mode, Aes_key_type keylenn);
    iv_aes      <= iv;
    key_aes     <= key;
    mode_aes    <= mode;
    aeskeylen   <= keylenn;
    decrypt_aes <= decrypt;
    ctr         <= 0;
    ctr_op      <= 0;
    started     <= False;
	blockaes.keygen_start(key, keylenn);

  endmethod

  method Action set_inp(Bit#(64) intext) if(bram_inp.notFull);
    if(ctr==0) begin
      inp <= intext;
      ctr <= ~ctr;
    end
    else begin
      bram_inp.enq({inp, intext}); // MSB INPUT FIRST
      //bram_inp.enq({intext,inp});// LSB INPUT FIRST
      ctr <= ~ctr;
    end
  endmethod

  method Action end_of_text();
    blockaes.end_of_text();
    bram_inp.clear();
    started <= False;
  endmethod

  method Bool can_take_inp();
    if(outbuf_ctr>=8) begin
      return False;
    end
    else begin
      return True;
    end
  endmethod

  method Bool outp_ready();
    return op_ready;
  endmethod

  method ActionValue#(Bit#(64)) get() if(op_ready);
    $display("AES bramfifo GET");
    if(ctr_op==0) begin
      ctr_op <= ~ctr_op;
      return op[63:0];
    end
    else begin
      ctr_op <= ~ctr_op;
      op_ready <= False;
      outbuf_ctr <= outbuf_ctr-1;
      return op[127:64];
    end
    //if(ctr_op==0) begin
    //  Bit#(128) res = bram_out.first();
    //  $display("AES bramfifo get LSB %h", res[63:0]);
    //  op <= res[127:64];
    //  bram_out.deq();
    //  ctr_op <= ~ctr_op;
    //  return res[63:0];
    //end
    //else begin
    //  ctr_op <= ~ctr_op;
    //  $display("AES bramfifo get LSB %h", op);
    //  outbuf_ctr <= outbuf_ctr - 1;
    //  return op;
    //end
  endmethod
endmodule

endpackage
