void aes_driver(void *buf, void *key, void *iv, int mode, int encdec, int keylen, uint64_t length){
        volatile uint64_t* aes_plaintext=(uint64_t*) 0x00021400;                                             
        volatile uint64_t* aes_key=(uint64_t*) 0x00021420;                                                   
        volatile uint64_t* aes_iv=(uint64_t*) 0x00021440;                                                    
        volatile uint64_t* aes_result=(uint64_t*) 0x00021460;                                                
        volatile uint8_t* aes_mode=(uint8_t*) 0x00021480;                                                    
        volatile uint8_t* aes_config=(uint8_t*) 0x00021483;                                                  
        volatile uint8_t* aes_encdec=(uint8_t*) 0x00021484;                                                  
        volatile uint8_t* aes_status=(uint8_t*) 0x00021485;                                                  
        volatile uint8_t* aes_keylen=(uint8_t*) 0x00021486;
        volatile uint8_t* aes_endian=(uint8_t*) 0x00021487;

        //printf("\nIV\n");
        //uint8_t* ivptr = iv;
        //for(int i=0; i<16; i++)
        //  printf(" %x", *(ivptr+i));
        //printf("\n");
        uint64_t *ptr2 = (long unsigned int)iv & ~(long unsigned int)0x7;
        int diff = ((char*)iv - (char*)ptr2)<<3;
        //printf("AES %d \n", diff);
        uint64_t temp, temp1, temp2;
        temp1 = *(ptr2+1);
        temp  = *(ptr2+2);
        temp2 = ((*((uint64_t*)(ptr2+1)))>>diff)|(temp<<(64-diff));
        *aes_iv = reverse_inp(temp2);
        //printf("AES iv %lx", temp2);
        temp1 = *(ptr2);
        temp = *(ptr2+1);
        temp2 = (temp1>>diff)|(temp<<(64-diff));
        *aes_iv = reverse_inp(temp2);
        //printf(" %lx\n", temp2);

        //*aes_iv = *(((uint64_t*)iv)+1);
        //*aes_iv = *((uint64_t*)iv);
        //*aes_iv = *(((uint64_t*)iv)+1);
        *aes_mode   = mode;
        *aes_encdec = encdec;
        *aes_keylen = keylen;
        //printf("AES %x %x %x", iv, ptr2, diff);
        //printf("AES %x", iv);
        //printf("AES %x", ((master_key+32*mk_index)));
        //printf("Master key: %lx %lx %lx %lx \n",*((uint64_t*)(master_key+32*mk_index)), *((uint64_t*)(master_key+32*mk_index+8)), *((uint64_t*)(master_key+32*mk_index+16)), *((uint64_t*)(master_key+32*mk_index+24)));

        //*aes_key = reverse_inp(*((uint64_t*)(key+24)));
        //*aes_key = reverse_inp(*((uint64_t*)(key+16)));
        //*aes_key = reverse_inp(*((uint64_t*)(key+8)));
        //*aes_key = reverse_inp(*((uint64_t*)(key)));
        
        uint64_t *ptrkey = (long unsigned int)key & ~(long unsigned int)0x7;
        diff = ((char*)key - (char*)ptrkey)<<3;

        printf("DIFF %x" , diff);
        if(diff==0){
          *aes_key = reverse_inp(*((uint64_t*)(key+24)));
          *aes_key = reverse_inp(*((uint64_t*)(key+16)));
          *aes_key = reverse_inp(*((uint64_t*)(key+8)));
          *aes_key = reverse_inp(*((uint64_t*)(key)));
        }
        else {
          printf("\n %lx %lx %lx %lx %lx\n", *(ptrkey+4), *(ptrkey+3), *(ptrkey+2), *(ptrkey+1), *(ptrkey));
          temp1 = *(ptrkey+4);
          temp = *(ptrkey+3);
          temp2 = (temp>>diff)|(temp1<<(64-diff));
          printf(" %lx", temp2);
          *aes_key = reverse_inp(temp2);
          temp1 = *(ptrkey+3);
          temp = *(ptrkey+2);
          temp2 = (temp>>diff)|(temp1<<(64-diff));
          printf(" %lx", temp2);
          *aes_key = reverse_inp(temp2);
          temp1 = *(ptrkey+2);
          temp = *(ptrkey+1);
          temp2 = (temp>>diff)|(temp1<<(64-diff));
          printf(" %lx", temp2);
          *aes_key = reverse_inp(temp2);
          temp1 = *(ptrkey+1);
          temp = *(ptrkey+0);
          temp2 = (temp>>diff)|(temp1<<(64-diff));
          printf(" %lx", temp2);
          *aes_key = reverse_inp(temp2);
        }

        *aes_endian = 1;
        *aes_config = 1;
        //printf("AES key: ");
        //for(int j=0; j<32; j++)
        //  printf("%x ",*(master_key+32*mk_index+j));
        //printf("\n");
        //printf("AES inp: ");
        //for(int j=0; j<32; j++)
        //  printf("%x ",buf[j]);
        //printf("\n%lx %lx %lx %lx", *((uint64_t*)(buf+0)), *((uint64_t*)(buf+8)), *((uint64_t*)(buf+16)), *((uint64_t*)(buf+24)));
        //printf("\n%lx %lx %lx %lx", reverse_inp(*((uint64_t*)(buf+0))), reverse_inp(*((uint64_t*)(buf+8))), reverse_inp(*((uint64_t*)(buf+16))), reverse_inp(*((uint64_t*)(buf+24))));
        for(int i=0; i<length; i+=8){
          //if((i&0xf) == 0){
          //  *aes_plaintext = (*((uint64_t*)(buf+i+8)));
          //  //*aes_plaintext = reverse_inp(*((uint64_t*)(buf+i+8)));
          //}
          //else {
          //  *aes_plaintext = (*((uint64_t*)(buf+i-8)));
          //  //*aes_plaintext = reverse_inp(*((uint64_t*)(buf+i-8)));
          //}
          *aes_plaintext = (*((uint64_t*)(buf+i)));
        }
        *aes_config = 0;
        *aes_endian = 0;
        while((*aes_status & 1) == 0);

        for(int i=0; i<length; i+=8){
          if((i&0xf)==0)
            *((uint64_t*)(buf+i+8)) = reverse_inp(*aes_result);
          else
            *((uint64_t*)(buf+i-8)) = reverse_inp(*aes_result);
        }
        //printf("\nAES outp: ");
        //printf("%lx %lx %lx %lx", *((uint64_t*)(buf+0)), *((uint64_t*)(buf+8)), *((uint64_t*)(buf+16)), *((uint64_t*)(buf+24)));
}

