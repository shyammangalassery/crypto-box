package KeyGen;

import rcon_gen :: *;
import sbox2 :: *;
import Vector :: *;
import BRAM :: *;
import BRAMCore :: *;

interface KeyGen_;
	method Action start(Bit#(32) key_in0, Bit#(32) key_in1, Bit#(32) key_in2, Bit#(32) key_in3, Bit#(32) key_in4, Bit#(32) key_in5, Bit#(32) key_in6, Bit#(32) key_in7, Aes_key_type keylen);
	method Action reqKey(Bit#(4) roundnumber);
	method ActionValue#(Bit#(128)) getKey(Bit#(4) roundnumber);
	method Bool isRoundKeyReady(Bit#(4) roundnum);
endinterface

typedef enum {
	Bit128, Bit192, Bit256
} Aes_key_type deriving(Bits, Eq, FShow); 

typedef enum {
	Key_request3, Key_request, Key_response, Idle
} Rg_State_type deriving(Bits, Eq, FShow);

typedef enum {
	Idle, Key_Gen_Start, SUB, Round_Keys, SUB_col_256, RoundKey0, Key_Gen_Start2
} KEY_State_type deriving(Bits, Eq, FShow);

function BRAMRequest#(Bit#(128), Bit#(128)) makeRequest(Bool write, Bit#(128) addr, Bit#(128) data);
    return BRAMRequest {
        write : write,
        responseOnWrite : False,
        address : addr,
        datain : data
    };
endfunction

module mkKeyGen#(Sbox2 sb)(KeyGen_);
	Reg#(Aes_key_type) key_length <- mkReg(Bit128);
	Reg#(Bool) key_saved <- mkReg(False);
	Reg#(Bool) rg_keyreq <- mkReg(False);
	
	Reg#(Bit#(4)) counter <-mkReg(8);
	Reg#(Bit#(4)) round_number <- mkReg(4);
	Reg#(Bool) round_toggle_192 <- mkReg(False);
	 
	Vector #(15, Reg#(Bool)) round_key_done <- replicateM(mkReg(False));

	BRAM_Configure cfg = defaultValue;
	cfg.allowWriteResponseBypass = False;

	BRAM_PORT#(Bit#(4), Bit#(32)) save_key0 <- mkBRAMCore1(16, False);
	BRAM_PORT#(Bit#(4), Bit#(32)) save_key1 <- mkBRAMCore1(16, False);
	BRAM_PORT#(Bit#(4), Bit#(32)) save_key2 <- mkBRAMCore1(16, False);
	BRAM_PORT#(Bit#(4), Bit#(32)) save_key3 <- mkBRAMCore1(16, False);
	//Reg#(Bit#(32)) key_in0_temp <- mkReg(32), key_in1_temp <- mkReg(32), key_in2_temp <- mkReg(32), key_in3_temp <- mkReg(32);
	//Reg#(Bit#(32)) key_in4_temp <- mkReg(32), key_in5_temp <- mkReg(32), key_in6_temp <- mkReg(32), key_in7_temp <- mkReg(32);
	Reg#(Bit#(32)) temp_key0 <- mkReg(32), temp_key1 <- mkReg(32), temp_key2 <- mkReg(32), temp_key3 <- mkReg(32);

	//Vector #(15, Reg#(Bit#(32))) save_key0 <- replicateM(mkRegU);
	//Vector #(15, Reg#(Bit#(32))) save_key1 <- replicateM(mkRegU);
	//Vector #(15, Reg#(Bit#(32))) save_key2 <- replicateM(mkRegU);
	//Vector #(15, Reg#(Bit#(32))) save_key3 <- replicateM(mkRegU);

	RconRom rcongen <- mkRconRom;

	Reg#(Bit#(32)) rot_prev_key <- mkReg(32);
	Reg#(Bit#(32)) prev_key0 <- mkReg(32), prev_key1 <- mkReg(32), prev_key2 <- mkReg(32), prev_key3 <- mkReg(32);
	Reg#(Bit#(32)) prev_key4 <- mkReg(32), prev_key5 <- mkReg(32), prev_key6 <- mkReg(32), prev_key7 <- mkReg(32);
	Reg#(Bit#(8)) sbox_out <- mkReg(8);
	Reg#(Bit#(32)) rcon <- mkReg(32), last_col_sub_rot <- mkReg(32);
	Reg#(Bit#(4)) rci <- mkReg(0);
	Reg#(Bit#(32)) aes256_subcol4 <- mkReg(32);

	Reg#(KEY_State_type) rg_state <- mkReg(Idle);
	Reg#(Rg_State_type) rg_state_key <- mkReg(Idle);
	Reg#(Bit#(4)) maxrounds <- mkReg(10);

	
	rule round_start (rg_state == Key_Gen_Start);
    //$display("Generating The Keys\n");
		counter <= 0;
		rcon <= {rcongen.read(rci), 24'b00};
		round_key_done[0] <= True;

		//save_key0[0] <= prev_key0;
		//save_key1[0] <= prev_key1;
		//save_key2[0] <= prev_key2;
		//save_key3[0] <= prev_key3;
		save_key0.put(True, 0, prev_key0);
		save_key1.put(True, 0, prev_key1);
		save_key2.put(True, 0, prev_key2);
		save_key3.put(True, 0, prev_key3);
		rg_state <= Key_Gen_Start2;
		//if (key_length == Bit128) begin
		//	sbox_out <= sb.getbyte(prev_key3[23:16]);
		//	rot_prev_key <= {prev_key3[23:0],prev_key3[31:24]};
		//end
		//else if(key_length == Bit192) begin
		//	//save_key0[1] <= prev_key4;
		//	//save_key1[1] <= prev_key5;
		//	save_key0.put(True, 1, prev_key4);
		//	save_key1.put(True, 1, prev_key5);
		//	sbox_out <= sb.getbyte(prev_key5[23:16]);
		//	rot_prev_key <= {prev_key5[23:0],prev_key5[31:24]};
		//	round_toggle_192 <= False;
		//end
		//else if(key_length == Bit256) begin
		//	//save_key0[1] <= prev_key4;
        //    //            save_key1[1] <= prev_key5;
		//	//save_key2[1] <= prev_key6;
		//	//save_key3[1] <= prev_key7;
		//	save_key0.put(True, 0, prev_key4);
		//	save_key1.put(True, 0, prev_key5);
		//	save_key2.put(True, 0, prev_key6);
		//	save_key3.put(True, 0, prev_key7);
		//	round_key_done[1] <= True;
        //                sbox_out <= sb.getbyte(prev_key7[23:16]);
        //                rot_prev_key <= {prev_key7[23:0],prev_key7[31:24]};
		//	//$display("%h%h%h%h,   0",prev_key0,prev_key1,prev_key2,prev_key3);
		//	//$display("%h%h%h%h,   1",prev_key4,prev_key5,prev_key6,prev_key7);
		//end
		//rg_state <= SUB;
	endrule

	rule round_start2 (rg_state == Key_Gen_Start2);
		if (key_length == Bit128) begin
      let sbout <- sb.getbyte(prev_key3[23:16], False);
      sbox_out <= sbout;
			//sbox_out <= sb.getbyte(prev_key3[23:16]);
			rot_prev_key <= {prev_key3[23:0],prev_key3[31:24]};
		end
		else if(key_length == Bit192) begin
			//save_key0[1] <= prev_key4;
			//save_key1[1] <= prev_key5;
			save_key0.put(True, 1, prev_key4);
			save_key1.put(True, 1, prev_key5);
      let sbout <- sb.getbyte(prev_key5[23:16], False);
      sbox_out <= sbout;
			//sbox_out <= sb.getbyte(prev_key5[23:16]);
			rot_prev_key <= {prev_key5[23:0],prev_key5[31:24]};
			round_toggle_192 <= False;
		end
		else if(key_length == Bit256) begin
			//save_key0[1] <= prev_key4;
            //            save_key1[1] <= prev_key5;
			//save_key2[1] <= prev_key6;
			//save_key3[1] <= prev_key7;
			save_key0.put(True, 1, prev_key4);
			save_key1.put(True, 1, prev_key5);
			save_key2.put(True, 1, prev_key6);
			save_key3.put(True, 1, prev_key7);
			round_key_done[1] <= True;
      let sbout <- sb.getbyte(prev_key7[23:16], False);
      sbox_out <= sbout;
      //                  sbox_out <= sb.getbyte(prev_key7[23:16]);
                        rot_prev_key <= {prev_key7[23:0],prev_key7[31:24]};
			$display("PREV KEY %h%h%h%h,   0",prev_key0,prev_key1,prev_key2,prev_key3);
			//$display("%h%h%h%h,   1",prev_key4,prev_key5,prev_key6,prev_key7);
		end
		rg_state <= SUB;
	endrule

	rule subKey (rg_state == SUB);
		if(counter == 0) begin
			last_col_sub_rot[31:24] <= sbox_out ^ rcon[31:24];
      let sbout <- sb.getbyte(rot_prev_key[23:16], False);
      sbox_out <= sbout;
			//sbox_out <= sb.getbyte(rot_prev_key[23:16]);
		end
		if(counter == 1) begin
			last_col_sub_rot[23:16] <= sbox_out ^ rcon[23:16];
      let sbout <- sb.getbyte(rot_prev_key[15:8], False);
      sbox_out <= sbout;
			//sbox_out <= sb.getbyte(rot_prev_key[15:8]);
		end
		if(counter == 2) begin
			last_col_sub_rot[15:8] <= sbox_out ^ rcon[15:8];
      let sbout <- sb.getbyte(rot_prev_key[7:0], False);
      sbox_out <= sbout;
			//sbox_out <= sb.getbyte(rot_prev_key[7:0]);
		end
		if(counter == 3) begin
			last_col_sub_rot[7:0] <= sbox_out ^ rcon[7:0];
			if(key_length == Bit256 && round_number != maxrounds) begin
				let col_sub_rot = {last_col_sub_rot[31:8], sbox_out ^ rcon[7:0]};
				let temp_col = prev_key0^prev_key1^prev_key2^prev_key3^col_sub_rot;
        let sbout <- sb.getbyte(temp_col[31:24], False);
        sbox_out <= sbout;
				//sbox_out <= sb.getbyte(temp_col[31:24]);
				aes256_subcol4 <= temp_col;
				rg_state <= SUB_col_256;
				//rg_state <= Round_Keys;
			end
			else
				rg_state <= Round_Keys;
			rci <= rci + 1;
		end
    //$display("SUB %h %h %h", last_col_sub_rot, sbox_out, rot_prev_key);

		counter <= counter + 1;
	endrule

	rule aes256subcol (rg_state == SUB_col_256);
	//$display("Iam in subbytes of column 4, %d",round_number);
		if(counter == 4) begin
			aes256_subcol4[31:24] <= sbox_out;
      let sbout <- sb.getbyte(aes256_subcol4[23:16], False);
      sbox_out <= sbout;
			//sbox_out <= sb.getbyte(aes256_subcol4[23:16]);
		end
		if(counter == 5) begin
			aes256_subcol4[23:16] <= sbox_out;
      let sbout <- sb.getbyte(aes256_subcol4[15:8], False);
      sbox_out <= sbout;
			//sbox_out <= sb.getbyte(aes256_subcol4[15:8]);
		end
		if(counter == 6) begin
			aes256_subcol4[15:8] <= sbox_out;
      let sbout <- sb.getbyte(aes256_subcol4[7:0], False);
      sbox_out <= sbout;
			//sbox_out <= sb.getbyte(aes256_subcol4[7:0]);
		end
		if(counter == 7) begin
			aes256_subcol4[7:0] <= sbox_out;
			rg_state <= Round_Keys;
		end
		counter <= counter +1;
	endrule

	rule roundKey (rg_state == Round_Keys);
		counter <= 0;
		rcon <= {rcongen.read(rci), 24'b00};

		let key0 = prev_key0^last_col_sub_rot;
		let key1 = prev_key1^key0;
		let key2 = prev_key2^key1;
		let key3 = prev_key3^key2;
		let key4 = prev_key4^key3;

		if (key_length == Bit256)
			key4 = prev_key4^aes256_subcol4;

		let key5 = prev_key5^key4;
		let key6 = prev_key6^key5;
		let key7 = prev_key7^key6;

		if(round_number <= maxrounds) begin
			if (key_length == Bit128) begin
				save_key0.put(True, round_number, key0);
				save_key1.put(True, round_number, key1);
				save_key2.put(True, round_number, key2);
				save_key3.put(True, round_number, key3);
				round_key_done[round_number] <= True;
				round_number <= round_number+1;
        //$display("%h %h %h %h %h %h %h %h %h", key0, key1, key2, key3, prev_key0, prev_key1, prev_key2, prev_key3, last_col_sub_rot);
				rg_state <= RoundKey0;

        //prev_key0 <= key0;
			  //prev_key1 <= key1;
			  //prev_key2 <= key2;
			  //prev_key3 <= key3;
			  //prev_key4 <= key4;
			  //prev_key5 <= key5;
			  //prev_key6 <= key6;
			  //prev_key7 <= key7;

			end
			else if(key_length == Bit192) begin
				if(round_toggle_192) begin
					save_key0.put(True, round_number, key0);
					save_key1.put(True, round_number, key1);
					save_key2.put(True, round_number, key2);
					save_key3.put(True, round_number, key3);
					rg_state <= RoundKey0;
					//save_key0.put(True, round_number+1, key4);
					//save_key1.put(True, round_number+1, key5);
					//round_key_done[round_number] <= True;
					//round_number <= round_number + 1;
					//round_toggle_192 <= False;
				end
				else begin
					save_key2.put(True, round_number, key0);
					save_key3.put(True, round_number, key1);
					rg_state <= RoundKey0;
					//save_key0.put(True, round_number+1, key2);
					//save_key1.put(True, round_number+1, key3);
					//save_key2.put(True, round_number+1, key4);
					//save_key3.put(True, round_number+1, key5);
					//round_key_done[round_number] <= True;
					//round_key_done[round_number+1] <= True;
					//round_number <= round_number + 2;
					//round_toggle_192 <= True;
				end
				//sbox_out <= sb.getbyte(key5[23:16]);
				//rot_prev_key <= {key5[23:0],key5[31:24]};
				//rg_state<=SUB;
			end
			else if(key_length == Bit256) begin
				save_key0.put(True, round_number, key0);
				save_key1.put(True, round_number, key1);
				save_key2.put(True, round_number, key2);
				save_key3.put(True, round_number, key3);
				rg_state <= RoundKey0;
				//round_key_done[round_number] <= True;
				//if(round_number != maxrounds) begin
				//	save_key0.put(True, round_number+1, key4);
				//	save_key1.put(True, round_number+1, key5);
				//	save_key2.put(True, round_number+1, key6);
				//	save_key3.put(True, round_number+1, key7);
				//	sbox_out <= sb.getbyte(key7[23:16]);
				//	rot_prev_key <= {key7[23:0],key7[31:24]};
				//	round_key_done[round_number+1] <= True;
				//	round_number <= round_number + 2;
				//	rg_state <= SUB;
				//end
				//else begin
				//	rg_state <= Idle;
				//	key_saved <= True;
				//end
				$display("Inverse Key gen %h %h", round_number, {key0, key1, key2, key3});
			end
			//prev_key0 <= key0;
			//prev_key1 <= key1;
			//prev_key2 <= key2;
			//prev_key3 <= key3;
			//prev_key4 <= key4;
			//prev_key5 <= key5;
			//prev_key6 <= key6;
			//prev_key7 <= key7;
		end
		else begin
			rg_state <= Idle;
			key_saved <= True;
		  //$display("Key gen DONEEEE");
		end
		//$display("Inverse Key gen %h", round_number);
	endrule

	rule roundKey2(rg_state == RoundKey0);
		counter <= 0;
		rcon <= {rcongen.read(rci), 24'b00};

		let key0 = prev_key0^last_col_sub_rot;
		let key1 = prev_key1^key0;
		let key2 = prev_key2^key1;
		let key3 = prev_key3^key2;
		let key4 = prev_key4^key3;

		if (key_length == Bit256)
			key4 = prev_key4^aes256_subcol4;

		let key5 = prev_key5^key4;
		let key6 = prev_key6^key5;
		let key7 = prev_key7^key6;

		if(round_number <= maxrounds) begin
			if(key_length == Bit192) begin
				if(round_toggle_192) begin
					save_key0.put(True, round_number+1, key4);
					save_key1.put(True, round_number+1, key5);
					round_key_done[round_number] <= True;
					round_number <= round_number + 1;
					round_toggle_192 <= False;
				end
				else begin
					save_key0.put(True, round_number+1, key2);
					save_key1.put(True, round_number+1, key3);
					save_key2.put(True, round_number+1, key4);
					save_key3.put(True, round_number+1, key5);
					round_key_done[round_number] <= True;
					round_key_done[round_number+1] <= True;
					round_number <= round_number + 2;
					round_toggle_192 <= True;
				end
        let sbout <- sb.getbyte(key5[31:24], False);
        sbox_out <= sbout;
				//sbox_out <= sb.getbyte(key5[23:16]);
				rot_prev_key <= {key5[23:0],key5[31:24]};
				rg_state<=SUB;
			end
			else if(key_length == Bit256) begin
				round_key_done[round_number] <= True;
				if(round_number != maxrounds) begin
					save_key0.put(True, round_number+1, key4);
					save_key1.put(True, round_number+1, key5);
					save_key2.put(True, round_number+1, key6);
					save_key3.put(True, round_number+1, key7);
          let sbout <- sb.getbyte(key7[23:16], False);
          sbox_out <= sbout;
					//sbox_out <= sb.getbyte(key7[23:16]);
					rot_prev_key <= {key7[23:0],key7[31:24]};
					round_key_done[round_number+1] <= True;
					round_number <= round_number + 2;
					rg_state <= SUB;
					$display("KEYGEN round number %h, %h, %h", round_number+1, maxrounds, {key4, key5, key6, key7});
				end
				else begin
					rg_state <= Idle;
					key_saved <= True;
				end
			end
      else if(key_length == Bit128) begin
        rg_state <= SUB;
        let sbout <- sb.getbyte(key3[23:16],False);                                                 
        sbox_out <= sbout;                                                                          
        rot_prev_key <= {key3[23:0],key3[31:24]};                                                   
      end
			prev_key0 <= key0;
			prev_key1 <= key1;
			prev_key2 <= key2;
			prev_key3 <= key3;
			prev_key4 <= key4;
			prev_key5 <= key5;
			prev_key6 <= key6;
			prev_key7 <= key7;
		end
		else begin
			rg_state <= Idle;
			key_saved <= True;
		  //$display("KEYGEN DONE");
		end
		//$display("Inverse Key gen %h", round_number);
	endrule

	method Action start(Bit#(32) key_in0, Bit#(32) key_in1, Bit#(32) key_in2, Bit#(32) key_in3, Bit#(32) key_in4, Bit#(32) key_in5, Bit#(32) key_in6, Bit#(32) key_in7, Aes_key_type keylen) if(rg_state == Idle);
		//$display("KEYGEN: Start");
		//save_key0.put(False, 0, ?);
		//save_key1.put(False, 0, ?);
		//save_key2.put(False, 0, ?);
		//save_key3.put(False, 0, ?);
		//key_in0_temp <= key_in0;
		//key_in1_temp <= key_in1;
		//key_in2_temp <= key_in2;
		//key_in3_temp <= key_in3;
		//key_in4_temp <= key_in4;
		//key_in5_temp <= key_in5;
		//key_in6_temp <= key_in6;
		//key_in7_temp <= key_in7;
		//rg_state_key <= Key_request;
		//key_length <= keylen;
		Integer i;
		for(i = 0 ; i < 15 ; i = i +1)
			(round_key_done[i]) <= False;
		rg_state <= Key_Gen_Start;
		case(keylen)
			Bit128 : begin 
				maxrounds <= 10;
				prev_key0 <= key_in4;
				prev_key1 <= key_in5;
				prev_key2 <= key_in6;
				prev_key3 <= key_in7;
				round_number <= 4'b0001;

			end
			Bit192 : begin
				maxrounds <= 12;
				prev_key0 <= key_in2;
				prev_key1 <= key_in3;
				prev_key2 <= key_in4;
				prev_key3 <= key_in5;
				prev_key4 <= key_in6;
				prev_key5 <= key_in7;
				round_number <= 4'b0001;
			end
			Bit256 : begin
				maxrounds <= 14;
				prev_key0 <= key_in0;
				prev_key1 <= key_in1;
				prev_key2 <= key_in2;
				prev_key3 <= key_in3;
				prev_key4 <= key_in4;
				prev_key5 <= key_in5;
				prev_key6 <= key_in6;
				prev_key7 <= key_in7;
				round_number <= 4'b0010;
			end
		endcase

		key_length <= keylen;
		rci <=0;
	endmethod
	
	method Action reqKey(Bit#(4) roundnumber);
		save_key0.put(False, roundnumber, 0);
		save_key1.put(False, roundnumber, 0);
		save_key2.put(False, roundnumber, 0);
		save_key3.put(False, roundnumber, 0);
		//rg_keyreq <= True;
		//$display("reqKey %h", roundnumber);
	endmethod

	method ActionValue#(Bit#(128)) getKey(Bit#(4) roundnumber);
		//rg_keyreq <= False;
    //let temp = save_key0.read();
		//$display("GetKey %h %h", roundnumber, temp);
		//return {temp, save_key1.read(), save_key2.read(), save_key3.read()};
		return {save_key0.read(), save_key1.read(), save_key2.read(), save_key3.read()};
	endmethod

	method Bool isRoundKeyReady(Bit#(4) roundnum);
		return round_key_done[roundnum];
	endmethod

endmodule

endpackage
