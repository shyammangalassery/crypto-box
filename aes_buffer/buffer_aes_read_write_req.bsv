package buffer_aes_read_write_req;
  import buffer_aes_bramfifo :: *;
  import AXI4_Lite_Types::*;
  import AXI4_Lite_Fabric::*;
  import AXI4_Types::*;
  import AXI4_Fabric::*;
  import Semi_FIFOF::*;
  import GetPut::*;
  import FIFO::*;
  import Clocks::*;
  import BUtils::*;
  import device_common::*;
  
  interface UserInterface#(numeric type addr_width);
  	method ActionValue#(Tuple2#(Bit#(64),Bool)) read_req (Bit#(addr_width) addr, AccessSize size);
  	method ActionValue#(Bool) write_req(Bit#(addr_width) addr, Bit#(64) data);
	  method Bool outp_ready();
    method Bool can_take_inp();
  endinterface
  
  //`define InputText 'b00;
  //`define KeyReg 'h20;
  //`define IVReg 'h40;
  //`define OutputReg 'h60;
  //`define ModeReg 'h80;
  //`define ConfigurationReg 'h83;
  //`define EncDecReg 'h84;
  //`define StatusReg 'h85;
  //`define KeyLenReg 'h86;
 
  `include "buffer_aes_include.defines"
  typedef enum {
  	Idle, Compute, Ready
  } AES_State_type deriving(Bits, Eq, FShow);
  
  module mkuser_aesbuf(UserInterface#(addr_width))
  provisos(Add#(a__, 8, addr_width));
  	AES_buffer#(8) aes_b <- mkAes_Buffer;
  	Reg#(Bit#(256)) rg_key <- mkReg(0);
  	Reg#(Bit#(128)) rg_iv <- mkReg(0);
    Reg#(Mode) rg_mode <- mkReg(ECB);
  	Reg#(Bit#(128)) rg_input_text <- mkReg(0);
  	Reg#(Bit#(1)) rg_config <- mkReg(0);
  	Reg#(Bool) rg_encdec <- mkReg(False);
  	Reg#(Bit#(1)) rg_endian <- mkReg(0);
  	Reg#(Aes_key_type) rg_keylen <- mkReg(Bit128);
  
  	Reg#(Bit#(2)) key_index <- mkReg(0);
  	Reg#(Bit#(1)) iv_index <- mkReg(0);
  
  	method ActionValue#(Tuple2#(Bit#(64),Bool)) read_req (Bit#(addr_width) addr, AccessSize size);
      $display($time,"\tAES: Read Req: Addr: %h", addr); 
      if(truncate(addr) == `ConfigurationReg) begin
  			return tuple2(duplicate({7'd0, rg_config}), True);
      end
  		else if(truncate(addr) == `OutputReg) begin
        let val <- aes_b.get();
        $display("AES: Output LSB %h", val);
  			return tuple2(val, True);
  		end
      else if(truncate(addr) == `StatusReg) begin
        $display("AES: Status read response %b", aes_b.outp_ready);
        Bit#(64) read_resp= duplicate({7'd0, pack(aes_b.outp_ready)});
  			return tuple2(read_resp, True);
      end
      else begin
  			return tuple2(?, False);
      end
  	endmethod
  
  	method ActionValue#(Bool) write_req(Bit#(addr_width) addr, Bit#(64) data);
      $display($time,"\tAES: Write Req: Addr: %h Data: %h", addr, data); 
  		Bool lv_success= True;
  		if(truncate(addr) == `ConfigurationReg) begin
  			rg_config <= data[0];
        if(data[0]==1)
          aes_b.set_iv_key_mode_len(rg_key, rg_iv, rg_encdec, rg_mode, rg_keylen);
        else
          aes_b.end_of_text;
  		end
  		else if(truncate(addr) == `EncDecReg) begin
  			rg_encdec <= unpack(data[0]);
  		end
  		else if(truncate(addr) == `KeyLenReg) begin
        if(data[1:0]==2'b01) begin
          let keyl = Bit128;
  			  rg_keylen <= keyl;
        end
        else if(data[1:0]==2'b10) begin
          let keyl = Bit192;
  			  rg_keylen <= keyl;
        end
        else if(data[1:0]==2'b11) begin
          let keyl = Bit256;
  			  rg_keylen <= keyl;
        end
  		end
  		else if(truncate(addr) == `ModeReg) begin
        case(data[2:0])
          3'b000: begin
            Mode md = ECB;
            rg_mode <= md;
            end
          3'b001: begin
            Mode md = CBC;
            rg_mode <= md;
            end
          3'b010: begin
            Mode md = CTR;
            rg_mode <= md;
            end
          3'b011: begin
            Mode md = CFB;
            rg_mode <= md;
            end
          3'b100: begin
            Mode md = OFB;
            rg_mode <= md;
            end
        endcase
  		end
  		else if(truncate(addr) == `KeyReg) begin
  			key_index<= key_index+1;
        if(key_index==0) begin
  				rg_key[63:0] <= data;
          $display("AES: Key LSB %h", data);
        end
        else if(key_index==1) begin
  				rg_key[127:64] <= data;
          $display("AES: Key LSB %h", data);
        end
        else if(key_index==2) begin
  				rg_key[191:128] <= data;
          $display("AES: Key LSB %h", data);
        end
        else begin
  				rg_key[255:192] <= data;
          $display("AES: Key MSB %h", data);
        end
  		end
  		else if(truncate(addr) == `IVReg) begin
  			iv_index<= ~iv_index;
        if(iv_index==0) begin
  				rg_iv[63:0] <= data;
          $display("AES: IV LSB %h", data);
        end
        else begin
  				rg_iv[127:64] <= data;
          $display("AES: IV MSB %h", data);
        end
      end
  		else if(truncate(addr) == `InputText) begin
        if(rg_endian==0) begin
          aes_b.set_inp(data);
          $display("AES: Input 1");
        end
        else begin
          aes_b.set_inp({data[7:0], data[15:8], data[23:16], data[31:24], data[39:32], data[47:40], data[55:48], data[63:56]});
          $display("AES: Input 2");
        end
        $display("AES: Input %h", data);
  		end
      else if(truncate(addr) == `EndianReg) begin
        rg_endian <= data[0];
        $display("AES: Endian %h", data);
      end
  		else
  			lv_success= False;
  		return lv_success;
  	endmethod
  
  	method can_take_inp = aes_b.can_take_inp;
  	method outp_ready   = aes_b.outp_ready;
  endmodule
  
  interface Ifc_aesbuf_axi4lite#(numeric type addr_width, 
                              numeric type data_width, 
                              numeric type user_width);
  	interface AXI4_Lite_Slave_IFC#(addr_width, data_width, user_width) slave; 
  	method Bool can_take_inp;
  	method Bool outp_ready;
 	endinterface
  
  module mkaesbuf_axi4lite#(Clock aes_clock, Reset aes_reset)
  															(Ifc_aesbuf_axi4lite#(addr_width,data_width,user_width))
  //same provisos for the aes
      provisos( Add#(data_width,0,64),
                Add#(a__, 8, addr_width)
        );
  
  	
  	Clock core_clock<-exposeCurrentClock;
  	Reset core_reset<-exposeCurrentReset;
  	Bool sync_required=(core_clock!=aes_clock);
  	AXI4_Lite_Slave_Xactor_IFC#(addr_width,data_width,user_width)  s_xactor <- mkAXI4_Lite_Slave_Xactor();
  
  	if(!sync_required)begin // If aes is clocked by core-clock.
  		UserInterface#(addr_width) aes<- mkuser_aesbuf();
  		//UserInterface#(addr_width) aes<- mkuser_aesbuf(clocked_by aes_clock, 
      //                                                             reset_by aes_reset, baudrate);
  		//capturing the read requests
  		rule capture_read_request;
  			let rd_req <- pop_o (s_xactor.o_rd_addr);
  			let {rdata,succ} <- aes.read_req(rd_req.araddr,unpack(rd_req.arsize));
  			let lv_resp= AXI4_Lite_Rd_Data {rresp:succ?AXI4_LITE_OKAY:AXI4_LITE_SLVERR, 
      	                                                      rdata: rdata, ruser: ?}; //TODO user?
  			s_xactor.i_rd_data.enq(lv_resp);//sending back the response
  		endrule              
  
  		// capturing write requests
  		rule capture_write_request;
  			let wr_req  <- pop_o(s_xactor.o_wr_addr);
  			let wr_data <- pop_o(s_xactor.o_wr_data);
  			let succ <- aes.write_req(wr_req.awaddr,wr_data.wdata);
      		let lv_resp = AXI4_Lite_Wr_Resp {bresp: succ?AXI4_LITE_OKAY:AXI4_LITE_SLVERR, buser: ?};
      		s_xactor.i_wr_resp.enq(lv_resp);//enqueuing the write response
  		endrule
  		interface slave = s_xactor.axi_side;
  	  method can_take_inp= aes.can_take_inp;
  	  method outp_ready= aes.outp_ready;
  	end
  	else begin // if core clock and aes_clock is different.
  		UserInterface#(addr_width) aes<- mkuser_aesbuf;
  		SyncFIFOIfc#(AXI4_Lite_Rd_Addr#(addr_width,user_width)) ff_rd_request <- 
  															mkSyncFIFOFromCC(3,aes_clock);
  		SyncFIFOIfc#(AXI4_Lite_Wr_Addr#(addr_width,user_width)) ff_wr_request <- 
  															mkSyncFIFOFromCC(3,aes_clock);
  		SyncFIFOIfc#(AXI4_Lite_Wr_Data#(data_width)) ff_wdata_request <- mkSyncFIFOFromCC(3,aes_clock);
  		SyncFIFOIfc#(AXI4_Lite_Rd_Data#(data_width,user_width)) ff_rd_response <- 
  															mkSyncFIFOToCC(3,aes_clock,aes_reset);
  		SyncFIFOIfc#(AXI4_Lite_Wr_Resp#(user_width)) ff_wr_response <- 
  															mkSyncFIFOToCC(3,aes_clock,aes_reset);
  		//capturing the read requests
  		rule capture_read_request;
  			let rd_req <- pop_o (s_xactor.o_rd_addr);
  			ff_rd_request.enq(rd_req);
  		endrule
  
  		rule perform_read;
  			let rd_req = ff_rd_request.first;
  			ff_rd_request.deq;
  			let {rdata,succ} <- aes.read_req(rd_req.araddr,unpack(rd_req.arsize));
  			let lv_resp= AXI4_Lite_Rd_Data {rresp:succ?AXI4_LITE_OKAY:AXI4_LITE_SLVERR, 
      	                                                      rdata: rdata, ruser: ?}; //TODO user?
  			ff_rd_response.enq(lv_resp);
  		endrule
  
  		rule send_read_response;
  			ff_rd_response.deq;
  			s_xactor.i_rd_data.enq(ff_rd_response.first);//sending back the response
  		endrule              
  
  		// capturing write requests
  		rule capture_write_request;
  			let wr_req  <- pop_o(s_xactor.o_wr_addr);
  			let wr_data <- pop_o(s_xactor.o_wr_data);
  			ff_wr_request.enq(wr_req);
  			ff_wdata_request.enq(wr_data);
  		endrule
  
  		rule perform_write;
  			let wr_req  = ff_wr_request.first;
  			let wr_data = ff_wdata_request.first;
  			let succ <- aes.write_req(wr_req.awaddr,wr_data.wdata);
      		let lv_resp = AXI4_Lite_Wr_Resp {bresp: succ?AXI4_LITE_OKAY:AXI4_LITE_SLVERR, buser: ?};
  			ff_wr_response.enq(lv_resp);
  		endrule
  
  		rule send_write_response;
  			ff_wr_response.deq;
      		s_xactor.i_wr_resp.enq(ff_wr_response.first);//enqueuing the write response
  		endrule
  		interface slave = s_xactor.axi_side;
  	  method can_take_inp= aes.can_take_inp;
  	  method outp_ready= aes.outp_ready;
  	end
  endmodule:mkaesbuf_axi4lite
  
  interface Ifc_aesbuf_axi4#(numeric type addr_width, 
                          numeric type data_width, 
                          numeric type user_width);
  	interface AXI4_Slave_IFC#(addr_width, data_width, user_width) slave; 
  	method Bool can_take_inp;
  	method Bool outp_ready;
 	endinterface
  
  module mkaesbuf_axi4#(Clock aes_clock, Reset aes_reset)
  															(Ifc_aesbuf_axi4#(addr_width,data_width,user_width))
  // same provisos for the aes
      provisos( Add#(data_width,0,64),
                Add#(a__, 8, addr_width)
        );
  
  	
  	Clock core_clock<-exposeCurrentClock;
  	Reset core_reset<-exposeCurrentReset;
  	Bool sync_required=(core_clock!=aes_clock);
  	AXI4_Slave_Xactor_IFC#(addr_width,data_width,user_width)  s_xactor <- mkAXI4_Slave_Xactor();
  
  	if(!sync_required)begin // If aes is clocked by core-clock.
  		UserInterface#(addr_width) aes<- mkuser_aesbuf();
  		//UserInterface#(addr_width) aes<- mkuser_aesbuf(clocked_by aes_clock, 
      //                                                             reset_by aes_reset, baudrate);
  		//capturing the read requests
  		rule capture_read_request;
  			let rd_req <- pop_o (s_xactor.o_rd_addr);
  			let {rdata,succ} <- aes.read_req(rd_req.araddr,unpack(truncate(rd_req.arsize)));
  			let lv_resp= AXI4_Rd_Data {rresp:succ?AXI4_OKAY:AXI4_SLVERR, rlast: True, rid: rd_req.arid,
      	                                                      rdata: rdata, ruser: ?}; //TODO user?
  			s_xactor.i_rd_data.enq(lv_resp);//sending back the response
  		endrule              
  
  		// capturing write requests
  		rule capture_write_request;
  			let wr_req  <- pop_o(s_xactor.o_wr_addr);
  			let wr_data <- pop_o(s_xactor.o_wr_data);
  			let succ <- aes.write_req(wr_req.awaddr,wr_data.wdata);
      	let lv_resp = AXI4_Wr_Resp {bresp: succ?AXI4_OKAY:AXI4_SLVERR, buser: ?, bid: wr_req.awid};
      	s_xactor.i_wr_resp.enq(lv_resp);//enqueuing the write response
  		endrule
  		interface slave = s_xactor.axi_side;
  	  method can_take_inp= aes.can_take_inp;
  	  method outp_ready= aes.outp_ready;
  	end
  	else begin // if core clock and aes_clock is different.
  		UserInterface#(addr_width) aes<- mkuser_aesbuf;
  		SyncFIFOIfc#(AXI4_Rd_Addr#(addr_width,user_width)) ff_rd_request <- 
  															mkSyncFIFOFromCC(3,aes_clock);
  		SyncFIFOIfc#(AXI4_Wr_Addr#(addr_width,user_width)) ff_wr_request <- 
  															mkSyncFIFOFromCC(3,aes_clock);
  		SyncFIFOIfc#(AXI4_Wr_Data#(data_width)) ff_wdata_request <- mkSyncFIFOFromCC(3,aes_clock);
  		SyncFIFOIfc#(AXI4_Rd_Data#(data_width,user_width)) ff_rd_response <- 
  											 	mkSyncFIFOToCC(3,aes_clock,aes_reset);
  		SyncFIFOIfc#(AXI4_Wr_Resp#(user_width)) ff_wr_response <- 
  															mkSyncFIFOToCC(3,aes_clock,aes_reset);
  		//capturing the read requests
  		rule capture_read_request;
  			let rd_req <- pop_o (s_xactor.o_rd_addr);
  			ff_rd_request.enq(rd_req);
  		endrule
  
  		rule perform_read;
  			let rd_req = ff_rd_request.first;
  			ff_rd_request.deq;
  			let {rdata,succ} <- aes.read_req(rd_req.araddr,unpack(truncate(rd_req.arsize)));
  			let lv_resp= AXI4_Rd_Data {rresp:succ?AXI4_OKAY:AXI4_SLVERR, rlast: True, rid: rd_req.arid,
      	                                                      rdata: rdata, ruser: ?}; //TODO user?
  			ff_rd_response.enq(lv_resp);
  		endrule
  
  		rule send_read_response;
  			ff_rd_response.deq;
  			s_xactor.i_rd_data.enq(ff_rd_response.first);//sending back the response
  		endrule              
  
  		// capturing write requests
  		rule capture_write_request;
  			let wr_req  <- pop_o(s_xactor.o_wr_addr);
  			let wr_data <- pop_o(s_xactor.o_wr_data);
  			ff_wr_request.enq(wr_req);
  			ff_wdata_request.enq(wr_data);
  		endrule
  
  		rule perform_write;
  			let wr_req  = ff_wr_request.first;
  			let wr_data = ff_wdata_request.first;
  			let succ <- aes.write_req(wr_req.awaddr,wr_data.wdata);
      		let lv_resp = AXI4_Wr_Resp {bresp: succ?AXI4_OKAY:AXI4_SLVERR, buser: ?, bid: wr_req.awid};
  			ff_wr_response.enq(lv_resp);
  		endrule
  
  		rule send_write_response;
  			ff_wr_response.deq;
      		s_xactor.i_wr_resp.enq(ff_wr_response.first);//enqueuing the write response
  		endrule
  		interface slave = s_xactor.axi_side;
  	  method can_take_inp= aes.can_take_inp;
  	  method outp_ready= aes.outp_ready;
  	end
  endmodule:mkaesbuf_axi4
endpackage
