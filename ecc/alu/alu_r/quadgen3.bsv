package quadgen3;
import Vector::*;
`include "details.bsv"
interface Quad_ifc;
method Action start(bit[1000:0] a); // input for quad block


method bit[`nl:0] result();
method int count();
endinterface: Quad_ifc

module mk_quad(Quad_ifc);
Reg# (bit[1000:0]) b<-mkReg(0);



Reg# (int) s<-mkReg(0);
Reg# (int) ct<-mkReg(0);

       





rule cyclce(s==1);  
bit[1500:0] x=0;
for(Integer i=0;i<`n;i=i+1)           // adds zero between each digit
	x[2*i]=b[i];
bit[`nl:0] x1 =(x[(`n*2):((`nl*2)+2-`m1)])^(x[(`n*2):((`nl*2)+2-`m2)])^(x[(`n*2):((`nl*2)+2-`m3)]); // modulo by polynomial
x1 = x1^(x[`nl:0])^(x[(`nl*2):`n])^(x[(`nl*2):`n]<<`m1)^(x[(`nl*2):`n]<<`m2)^(x[(`nl*2):`n]<<`m3);   // square is performed

b<=zeroExtend(x1);
if(ct==1)
	s<=4;  // when square is performed twice the rule is stopped
ct<=ct+1;
endrule


method Action start(a);



b<=a;
s<=1;

endmethod


method result()if(s==4);
return (b[`nl:0]); // output of the quad block
endmethod
method count()if(s==4);
return (ct);   // returns the total clock cycles
endmethod

endmodule:mk_quad 

module mktest(); //testbench
Reg#(int) state<-mkReg(0);
Quad_ifc m<-mk_quad;

rule go(state==0);
m.start(zeroExtend(571'b1110000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
 ));         // change the input according to the field
state<=2;
endrule

rule finish(state==2);
$display("Quad=%b",m.result()); // output is displayed in  bits.
$display("clock cycles=%d",m.count()); // clock cycles is displayed in  bits.
state<=4;
endrule

endmodule:mktest

endpackage:quadgen3
