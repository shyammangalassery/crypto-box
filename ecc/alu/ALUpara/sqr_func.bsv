//-------------FUNCTION TO PERFORM MODULO OPERATION BASED ON THE FIELD-------------------

package sqr_func;





function Bit#(600) sqr_fun(Bit#(1500) x, int n);  // input the bits to be reduced and the degree of the field

	
	int m1 =0;
	int m2 =0;
	int m3 =0;
	Bit#(600) ans = ?;
	case (n)      //choose the degrees of the polynomial based on the given field
		163 :  begin m1=7;m2=6;m3=3;
			end
		233 : begin m1=74;m2=0;m3=0;
			end
		283 : begin m1=12;m2=7;m3=5;
			end
		409 : begin m1=87;m2=0;m3=0;
			end
		571 : begin m1=10;m2=5;m3=2;
			end
	endcase	
        Bit#(n) x1 =(x[(n*2):(((n-1)*2)+2-m1)])^(x[(n*2):(((n-1)*2)+2-m2)])^(x[(n*2):(((n-1)*2)+2-m3)]); // selecting the bits exceed the bit limit in 2nd iteration
        x1 = x1^(x1<<m1)^(x1<<m2)^(x1<<m3)^(x[(n-1):0])^(x[((n-1)*2):n])^(x[((n-1)*2):n]<<m1)^(x[((n-1)*2):n]<<m2)^(x[((n-1)*2):n]<<m3);//xor the exceeded bits in 1st and 2nd iteration with  initial bits
	ans=x1;
	return ans;
endfunction

endpackage:sqr_func
