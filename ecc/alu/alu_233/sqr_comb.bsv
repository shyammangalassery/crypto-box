//--------------COMBINATIONAL SQUARER BLOCK-----------------------

package sqr;
import Vector::*;

interface Sqr_ifc;
method Action start(Bit#(233) a); // input for quad block


method Bit#(233) result();

endinterface: Sqr_ifc

module mk_sqr(Sqr_ifc);

Reg# (Bit#(233)) b<-mkReg(0);



Reg# (int) s<-mkReg(0);



       





rule cyclce(s==2);



endrule


method Action start(a);

Bit#(467) x=0; 
for(Integer i=0;i<233;i=i+1)            // adding zeros between each bit of input
	x[2*i]=a[i];
Bit#(233) x1 =(x[466:(((232)*2)+2-74)]); 
x1 = x1^(x1<<74)^(x1<<0)^(x1<<0)^(x[(232):0])^(x[((232)*2):233])^(x[((232)*2):233]<<74);   // performing modulo to reduce the oupt to 233 bits

b<=x1;

s<=4;


endmethod


method result()if(s==4);
return (b); // output of the quad block
endmethod

endmodule:mk_sqr 

module mktest(); //testbench
Reg#(int) state<-mkReg(0);
Sqr_ifc m<-mk_sqr;

rule go(state==0);
m.start(233'b11100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
 );         // change the input according to the field
state<=2;
endrule

rule finish(state==2);
$display("Quad=%b",m.result()); // output is displayed in  bits.

state<=4;
endrule
rule go2(state==4);
m.start( 233'b01010100000000000000000000000000000000000000000000000000000000000000000000000000000000101010000000000000000000000000000000000000000000000000000000000000000000001010100000000000000000000000000000000000000000000000000000000000000000000
);         // change the input according to the field
state<=7;
endrule

rule finish2(state==7);
$display("Quad=%b",m.result()); // output is displayed in  bits.

state<=8;
endrule

endmodule:mktest

endpackage:sqr
