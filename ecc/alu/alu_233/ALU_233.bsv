//ECC-ALU for binary field 233
package ALU_233;
import quad_cascade ::*;
import sqr::*;
import Hkmul::*;
import Vector::*;
typedef struct{
Bit#(3) muxA;
Bit#(3) muxB;
Bit#(2) muxC;
Bit#(2) muxD;
Bit#(4) quadSel;
} AluControlSig; //struct definition for control signals from control unit
interface ALU_ifc;
method Action start(Bit#(233) a_0,Bit#(233) a_1,Bit#(233) a_2,Bit#(233) a_3,Bit#(233) q_in,AluControlSig cons); //input five 233 bits and control signals
method Bit#(233) qout();
method Bit#(233) c0();
method Bit#(233) c1();


method bit status();

endinterface: ALU_ifc

module mk_ALU(ALU_ifc);

Reg# (Bit#(233)) a0<-mkReg(0);
Reg# (Bit#(233)) a1<-mkReg(0);
Reg# (Bit#(233)) a2<-mkReg(0);
Reg# (Bit#(233)) a3<-mkReg(0);
Reg# (Bit#(233)) mx<-mkReg(0);

Reg# (Bit#(233)) qin<-mkReg(0);
Reg# (Bit#(233)) ma<-mkReg(0);
Reg# (Bit#(233)) mb<-mkReg(0);

Reg# (Bit#(3)) muxA<-mkReg(0);
Reg# (Bit#(3)) muxB<-mkReg(0);
Reg# (Bit#(2)) muxC<-mkReg(0);
Reg# (Bit#(2)) muxD<-mkReg(0);


Reg# (int) s<-mkReg(0);

Vector#(6 ,Sqr_ifc) sq;                // instantiating 6 squarer blocks
for(Integer i3=0;i3<6;i3=i3+1)  
	sq[i3]<-mk_sqr;
Quadcas_ifc qd <-mk_quadcas; 
Hkmul_ifc hk <- mkHkmul;         
rule cycle1((s==1));  // square  of a0, a2 and a3 computed

sq[3].start(sq[0].result());
sq[4].start(sq[1].result());
sq[5].start(sq[2].result());
s<=2;
endrule
rule cycle2((s==2));  // select the inputs for the multiplier by control signals

case (muxA)
	3'b000 : ma<=a0;
	3'b001 : ma<=sq[0].result();
	3'b010 : ma<=a2;
	3'b011 : ma<=a0^a2;
	3'b100 : ma<=sq[3].result()^a1;
	3'b101 : ma<=a1;
	3'b110 : ma<=sq[4].result();
endcase
case (muxB)
	3'b000 : mb<=a1;
	3'b001 : mb<=sq[1].result();
	3'b010 : mb<=sq[1].result()^a2;
	3'b011 : mb<=a1^a3;
	3'b100 : mb<=sq[2].result()^a1^a3;
	3'b101 : mb<=a3;
	3'b110 : mb<=sq[5].result();
	3'b111 : mb<=sq[4].result();
endcase
s<=3;
endrule
rule hmult(s==3); //calling karatsuba multiplier  and performing modulo operation to reduce the output to 233 bits
let ans <- hk.mul(ma,mb);
Bit#(467) ml=zeroExtend( ans);
Bit#(233) m =(ml[466:(((232)*2)+2-74)]); // modulo by polynomial
m = m^(m<<74)^(ml[(232):0])^(ml[((232)*2):233])^(ml[((232)*2):233]<<74);   

mx<=m;

s<=4;
endrule


method Action start(a_0,a_1,a_2,a_3,q_in,cons);  // takes inputs A0, A1, A2  , Qin and  control signal
a0<=a_0;
a1<=a_1;
a2<=a_2;
a3<=a_3;
qin<=q_in;
muxA<=cons.muxA;
muxB<=cons.muxB;
muxC<=cons.muxC;
muxD<=cons.muxD;

qd.start2(q_in,cons.quadSel); // quad cascade block takes input 'qin' and 'quadSel' is to choose the appropriate quad power
sq[0].start(a_0);  // square of a0 , a1 and a2 computed
sq[1].start(a_1);
sq[2].start(a_2);

s<=1;

endmethod

method qout()if(s==4);            // output qout of quad cascade block

return (qd.result2()); 
endmethod


method c0()if(s==4);            // output c0 is muxed out

case (muxC)
	2'b00 : return(mx);
	2'b01 : return(mx^a2);
	2'b10 : return(sq[1].result());
	2'b11 : return(mx^a3);
	
endcase
endmethod
method c1()if(s==4);            // output c1 is muxed out

case (muxD)
	2'b00 : return(mx^a0);
	2'b01 : return(mx^a0^sq[2].result());
	2'b10 : return(sq[4].result());
	2'b11 : return(sq[3].result()^a1);
	
endcase
endmethod
method status();           // returns status of ALU block
if(s==4 || s==0)
	return(1'b1);
else
	return(1'b0);

endmethod

endmodule:mk_ALU

module mktestalu(); // testbench for alu
Reg#(int) state<-mkReg(0);
Reg#(int) s2<-mkReg(0);

ALU_ifc mta<-mk_ALU;

rule go((state==0));
AluControlSig cons;
cons.muxA=3'b000;
cons.muxB=3'b110;
cons.muxC=2'b00;
cons.muxD=2'b10;
cons.quadSel=4'b0001;
mta.start(233'b11100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
,(233'b11100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
),(233'b11100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
),(233'b11100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
),233'b11100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
,cons); //(inputs : a0,a1,a2,a3,qin and  control signal)
state<=2;
endrule

rule finish1( (state==2));
$display("qout=%b",mta.qout());
$display("c0=%b",mta.c0()); // c0
$display("c1=%b",mta.c1()); // c1



state<=4;
endrule

rule finish3((state==4));




state<=7;
endrule
endmodule:mktestalu

endpackage:ALU_233






