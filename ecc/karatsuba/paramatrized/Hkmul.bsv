/*	---------------------------------------------------------- PARAMATRIZED HYBRID KARATSUBA MULTIPLIER -------------------------------------------------------*/
package Hkmul;
import UniqueWrappers::*;

//===================================================== FIELD SIZE =================================================
`define size 233
`define outsize 465
//==================================================================================================================

import Gkmul ::*;

/*	function to autometically adjust the input to output width, similar to "zeroExtend()".	*/
function Bit#(y) fn_adjust(Bit#(x) inp);
  Bit#(TAdd#(x,y)) lv= zeroExtend(inp);
  return truncate(lv);
endfunction : fn_adjust


/*	hybrid karatsuba multiplier */
function Bit#(m) hkmul(Bit#(n) a, Bit#(n) b)	
	provisos( 	Add#(1, m, TMul#(2, n)),	// relationship between "m" & "n". i.e. output and input size respectively
				Div#(n,2,n_by_2)
			);

	Bit#(m) ans = ?;

	if(valueOf(n)<29)begin 						// BASE CASE, if size of input<29 then call gkmul
		ans = gkmul(a,b);
	end	
	else begin									// else proceed with simple karatsuba multilication algorithm.
		Integer sz = valueOf(n); 						// sz = size of input operand
		Integer l = valueOf(n_by_2);					// l = ceil(sz/2)

		Bit#(TDiv#(n,2)) aDash, bDash;								
				
		// aDash=ah+al;   bDash=bh+bl;	here "+" operation is the addition of two elements of a finite field (using XOR) 
		if(valueOf(n)%2 == 0)begin					// n is even		
			aDash = a[valueOf(n)-1:l] ^ a[l-1:0];    				
			bDash = b[valueOf(n)-1:l] ^ b[l-1:0];	
		end
		else begin									// n is odd
			aDash = a[valueOf(n)-1:l] ^ a[l-2:0];       
			aDash[l-1] = a[l-1];
			bDash = b[valueOf(n)-1:l] ^ b[l-2:0];		
			bDash[l-1] = b[l-1];
		end
		
		// splitting a into ah (i.e. a-high) and al (i.e. a-low)		
		Bit#(TDiv#(n,2)) 			al = a[l-1:0];				// sizeof(al) = ceil(n/2)
		Bit#(TSub#(n,TDiv#(n,2))) 	ah = a[valueOf(n)-1:l];		// sizeof(ah) = n - sizeOf(al)
		
		//splitting b into bh and bl
		Bit#(TDiv#(n,2)) 			bl = b[l-1:0];				// sizeof(al) = ceil(n/2)
		Bit#(TSub#(n,TDiv#(n,2))) 	bh = b[valueOf(n)-1:l];		// sizeof(ah) = n - sizeOf(al)
		
		//smaller multiplications (recursive calls)
		Bit#(TSub#(TMul#(2, TDiv#(n,2)), 1))			cp1 	= hkmul(al, bl); 				// low mul
		Bit#(TSub#(TMul#(2, TDiv#(n,2)), 1))			cp2 	= hkmul(aDash, bDash);			// center mul
		Bit#(TSub#(TMul#(2,TSub#(n,TDiv#(n,2))), 1))	cp3 	= hkmul(ah, bh);				// high mul

		// adjusting the output of the multiplications to output size
		Bit#(m) cP1= fn_adjust(cp1);
		Bit#(m) cP2= fn_adjust(cp2);
		Bit#(m) cP3= fn_adjust(cp3);
		
		//final shifting and addition (using XOR operation)
		ans = ( cP3<<(2*l) ) ^ ( (cP1 ^ cP2 ^ cP3)<<l ) ^ cP1; 		
	end
	
	return ans;
endfunction : hkmul





//--------------------------------- MODULE -------------------------------------
interface Hkmul_ifc;
	method ActionValue#(Bit#(`outsize)) mul(Bit#(`size) a, Bit#(`size) b); 
endinterface

(*synthesize*)
module mkHkmul(Hkmul_ifc);
	Wrapper2#(Bit#(`size), Bit#(`size), Bit#(`outsize)) hmult <- mkUniqueWrapper2(hkmul);
	
	method ActionValue#(Bit#(`outsize)) mul(Bit#(`size) a, Bit#(`size) b); 
		let ans <- hmult.func(a, b);
		return ans;
	endmethod
endmodule



endpackage : Hkmul
