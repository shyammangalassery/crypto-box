// Copyright (c) 2013-2016 Bluespec, Inc.  All Rights Reserved.

package Testbench;

// ================================================================
// Testbench to drive the sorting module.
// Feed n unsorted inputs to sorter,
// drain n sorted outputs and print
// ================================================================
// Project imports

import Utils      :: *;
import ControlUnit3_233 :: *;

// ================================================================
// Testbench module

//(* synthesize *)

module mkTestbench (Empty);

ControlUnit_IFC conUnit <- mkControlUnit; 

	Reg #(int) state <- mkReg (0);
	Reg #(Bit#(466)) result <- mkReg (0);
	Reg #(Bit#(1)) red <- mkReg (0);

	rule k_Input(state == 0);
			let k = 32'h10101010;
			conUnit.kVal(k);
			state <= 1;
	endrule

	rule out if (conUnit.resultStat() == 1);
		if (conUnit.resultStat == 1)
		result <= conUnit.resultOut();
		red <=1;
	endrule

	rule display (red == 1 && state == 1);
		$display("result X= %d",result[232:0]);
		$display("result X= %d",result[465:233]);
		state <= 2;
	endrule

	rule finish (state == 2);
		$finish;
	endrule

endmodule
// ================================================================

endpackage
