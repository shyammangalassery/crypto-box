package sequential_sha_engine;

`ifdef BRAMFIFO
  import BRAMFIFOF  :: *;
`else
  import FIFOF      :: *;
`endif

import GetPut       :: *;
import utils        :: *;

typedef enum{IDLE,HASH,OUTPUT} State deriving(Bits,Eq);

interface Ifc_sha_engine;
  method Action reset;
  method Bool ready;
  method Action input_engine(Bit#(256) pre_hash,Bit#(512) input_val);
  interface Get#(Bit#(256)) output_engine;
endinterface

function Bit#(32) rotr (Bit#(32) x,Integer n);
  Bit#(64) y={x,'d0} >> n;
  return y[63:32]|y[31:0];
endfunction:rotr

function Bit#(32) s0 (Bit#(32) x);
  return rotr(x,2)^rotr(x,13)^rotr(x,22);
endfunction:s0

function Bit#(32) s1 (Bit#(32) x);
  return rotr(x,6)^rotr(x,11)^rotr(x,25);
endfunction:s1

function Bit#(32) c0 (Bit#(32) x);
  return rotr(x,7)^rotr(x,18)^(x>>3);
endfunction:c0

function Bit#(32) c1 (Bit#(32) x);
  return rotr(x,17)^rotr(x,19)^(x>>10);
endfunction:c1

function Bit#(32) ch (Bit#(32) x, Bit#(32) y, Bit#(32) z);
  return (x & y)^(~x & z); 
endfunction:ch

function Bit#(32) maj (Bit#(32) x, Bit#(32) y, Bit#(32) z);
  return (x & y)^(x & z)^(y & z);
endfunction:maj

Bit#(32) cons_k[64]={
  'h428a2f98, 'h71374491, 'hb5c0fbcf, 'he9b5dba5, 'h3956c25b, 'h59f111f1, 'h923f82a4, 'hab1c5ed5,
  'hd807aa98, 'h12835b01, 'h243185be, 'h550c7dc3, 'h72be5d74, 'h80deb1fe, 'h9bdc06a7, 'hc19bf174,
  'he49b69c1, 'hefbe4786, 'h0fc19dc6, 'h240ca1cc, 'h2de92c6f, 'h4a7484aa, 'h5cb0a9dc, 'h76f988da,
  'h983e5152, 'ha831c66d, 'hb00327c8, 'hbf597fc7, 'hc6e00bf3, 'hd5a79147, 'h06ca6351, 'h14292967,
  'h27b70a85, 'h2e1b2138, 'h4d2c6dfc, 'h53380d13, 'h650a7354, 'h766a0abb, 'h81c2c92e, 'h92722c85,
  'ha2bfe8a1, 'ha81a664b, 'hc24b8b70, 'hc76c51a3, 'hd192e819, 'hd6990624, 'hf40e3585, 'h106aa070,
  'h19a4c116, 'h1e376c08, 'h2748774c, 'h34b0bcb5, 'h391c0cb3, 'h4ed8aa4a, 'h5b9cca4f, 'h682e6ff3,
  'h748f82ee, 'h78a5636f, 'h84c87814, 'h8cc70208, 'h90befffa, 'ha4506ceb, 'hbef9a3f7, 'hc67178f2 };

(* synthesize *)
(*mutually_exclusive = "hash,routput"*)
module mksequential_sha_engine(Ifc_sha_engine);
  
  Integer a=0;
  Integer b=1;
  Integer c=2;
  Integer d=3;
  Integer e=4;
  Integer f=5;
  Integer g=6;
  Integer h=7;

  Reg #(Bit#(6))  i         <- mkReg(0);
  Reg #(State)    state     <- mkReg(IDLE);
  Reg #(Bit#(32)) pre_comp  <- mkRegU;
  Reg #(Bit#(32)) w[16];
  Reg #(Bit#(32)) i_h[8];
  Reg #(Bit#(32)) initial_val[8];

  `ifdef BRAMFIFO
    FIFOF #(Bit#(256)) result_fifo<- mksizedBRAMFIFOF(2);
  `else
    FIFOF #(Bit#(256)) result_fifo <- mkSizedFIFOF(2);
  `endif

  for(Integer j=0;j<16;j=j+1)
    w[j]<-mkRegU;
  for(Integer j=0;j<8;j=j+1)begin
    i_h[j]<-mkRegU;
    initial_val[j]<-mkRegU;
  end

  rule hash(state==HASH);
  `ifdef verbose $display("SE:%8h State:Hash Action:Hash cycle-%8h",cur_cycle,i); `endif
    $display("SHA:%8h State:Hash Action:Hash cycle-%8h",cur_cycle,i);
    Bit#(32) tmp1=s1(i_h[e])+ch(i_h[e],i_h[f],i_h[g])+pre_comp;
    Bit#(32) tmp2=s0(i_h[a])+maj(i_h[a],i_h[b],i_h[c]);
    Bit#(32) tmp_e=i_h[d]+tmp1;
    Bit#(32) tmp_a=tmp1+tmp2;
    if(i<63)
      pre_comp<=i_h[g]+w[1]+cons_k[i+1];
    i_h[a]<=tmp_a;
    i_h[b]<=i_h[a];
    i_h[c]<=i_h[b];
    i_h[d]<=i_h[c];
    i_h[e]<=tmp_e;
    i_h[f]<=i_h[e];
    i_h[g]<=i_h[f];
    i_h[h]<=i_h[g];
    if(i<63)begin
      for(Integer j=0;j<15;j=j+1)
        w[j]<=w[j+1];
      w[15]<=w[0]+w[9]+c0(w[1])+c1(w[14]);
      i<=i+1;
    end
    else
      state<=OUTPUT;
  endrule

  rule routput(state==OUTPUT);
    Bit#(256) res={i_h[a]+initial_val[a],i_h[b]+initial_val[b],i_h[c]+initial_val[c],
                    i_h[d]+initial_val[d],i_h[e]+initial_val[e],i_h[f]+initial_val[f],
                    i_h[g]+initial_val[g],initial_val[h]+i_h[h]};
    result_fifo.enq(res);
    state<=IDLE;
    `ifdef verbose $display("SE:%8h State:Output Action:Output Digest:%64h", cur_cycle,res); `endif
    $display("SHA:%8h State:Output Action:Output Digest:%64h", cur_cycle,res);
  endrule

  method Bool ready;
    return(state==IDLE);
  endmethod

  method Action reset;
    i <= 0;
    state <= IDLE;
    result_fifo.clear;
  endmethod

  method Action input_engine(Bit#(256) pre_hash,Bit#(512) input_val) if(state==IDLE);
    $display("SHA: Engine input value %h", input_val);
    for(Integer j=0;j<16;j=j+1)
      w[j]<=input_val[512-(j*32)-1:480-(j*32)];
    pre_comp<=input_val[511:480]+cons_k[0]+pre_hash[31:0];
    i_h[a]<=pre_hash[255:224];
    i_h[b]<=pre_hash[223:192];
    i_h[c]<=pre_hash[191:160];
    i_h[d]<=pre_hash[159:128];
    i_h[e]<=pre_hash[127:96];
    i_h[f]<=pre_hash[95:64];
    i_h[g]<=pre_hash[63:32];
    i_h[h]<=pre_hash[31:0];
    initial_val[a]<=pre_hash[255:224];
    initial_val[b]<=pre_hash[223:192];
    initial_val[c]<=pre_hash[191:160];
    initial_val[d]<=pre_hash[159:128];
    initial_val[e]<=pre_hash[127:96];
    initial_val[f]<=pre_hash[95:64];
    initial_val[g]<=pre_hash[63:32];
    initial_val[h]<=pre_hash[31:0];
    i<=0;
    state<=HASH;
    `ifdef verbose $display("SE:%8h State:Idle Action:Input Ph:%64h M:%128h",cur_cycle,pre_hash,input_val); `endif
  endmethod
  interface Get output_engine;
    method ActionValue #(Bit#(256)) get;
    let x=result_fifo.first;
    $display("SHA: Engine output value %h", x);
    result_fifo.deq;
    return x;
    endmethod
  endinterface
endmodule:mksequential_sha_engine

endpackage:sequential_sha_engine
