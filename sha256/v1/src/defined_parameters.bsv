`ifdef PASS8
  `define Pass_length 64
`endif

`ifdef SYS32
  `define Reg_width 32
`endif
`ifdef SYS64
  `define Reg_width 64
`endif
`define MAXNum_of_engines 16
`define USERSPACE 0
`define PADDR 32
`define Result_fifo_size 2
`define PSHAADDR 64
`define Sha_accelerator_id 'b10
`define Number_of_hash_cycles 1000
`define Num_of_eng_in_grp Div#(512,`Pass_length)
`define Number_of_groups Div#(`MAXNum_of_engines,`Num_of_eng_in_grp)
`define Num_of_engines Mul#(`Number_of_groups,`Num_of_eng_in_grp)