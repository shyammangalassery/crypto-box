This repo consists the Bluespec code for implementation of SHA-256 algorithm.
```
/ccode - contains C implementation for the SHA256 used in verification.

/src - Contains the Bluespec source codes for the modules.
    /axi4 - Contains the Bluespec source codes for AXI4 fabric used as the communication interface
    /engine - contains the Bluespec source codes for the engines(accept a 512 bit message and a 256 bit value(a-h))
        ->sequential_sha_engine_unrolled.bsv - Sequential implementation for the sha algorithm.
              Optimisations employed-unrolling,precompute,quasi-pipelining
              Number of cycles taken-32
        ->sequential_sha_engine.bsv - Sequential implementation for the sha algorithm.
              Optimisations employed-precompute,quasi-pipelining
              Number of cycles taken-64
        ->pipeline_sha_engine.bsv - Pipelined implementation for the sha algorithm.
              Optimisations employed-precompute,quasi-pipelining
    /tcl - contains the tcl files required for vivado build.
    ->defined_parameters.bsv - contains the macros required for the accelerator.
    ->sha_accelerator_v1.bsv - The accelerator accepts two address values(read address and write address),no. of read bursts and password length in bytes as inputs.
                               Reads (starting from the read address),hashes and writes back onto memory.
    ->sha_accelerator_v2.bsv -(Partially completed) The accelerator expects the start address and the hash to look for. It reads(starting form the read address),performs
                               the specified number of hash cycles and compares the result with the given hash.

/testbench - contains Bluespec code for verification of the efficacy of the variants by cross verifying against the outputs obtained from the C implementation.

Use `engine` variable in the Makefile to switch between sequential and pipelined versions of the SHA engine.```